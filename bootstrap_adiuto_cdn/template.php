<?php
/**
 * @file
 * template.php
 */

function bootstrap_adiuto_cdn_preprocess_page(&$variables) {
  $variables['content_column_class'] = ' class="col-sm-8 col-lg-9"';
}


function bootstrap_adiuto_cdn_menu_link(array $variables) {
  $element = $variables ['element'];
  $sub_menu = '';
  #dsm($element);
  if ($element ['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $element['#localized_options']['html'] = 1;
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function bootstrap_adiuto_cdn_commerce_cart_empty_block() {
  return '<div class="">' . t('To plan your Mission, add some tasks.') . '<a href="/help#help-missions" alt="help"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>';
}


function bootstrap_adiuto_cdn_preprocess_select_as_checkboxes(&$variables) {
    $element = &$variables['element'];
    //Remove form-control class added to original "select" element
    if (($key = array_search('form-control', $element['#attributes']['class'])) !== false) {
        unset($element['#attributes']['class'][$key]);
    }    
}


/**
 * Allows sub-themes to alter the array used for colorizing text.
 *
 * @param array $texts
 *   An associative array containing the text and classes to be matched, passed
 *   by reference.
 *
 * @see _bootstrap_colorize_text()
 */
function bootstrap_adiuto_cdn_bootstrap_colorize_text_alter(&$texts) {

  // This matches the exact string: "My Unique Button Text".
  $texts['matches'][t('Locate')] = 'info';
  
    // This matches the exact string: "My Unique Button Text".
  $texts['matches'][t('To mission')] = 'success';
  $texts['matches']['<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ' . t('To mission')] = 'success';
  
  
  $texts['matches'][t('Abort mission')] = 'danger';
  
  // TODO: This is shit!
  $texts['matches'][t('Neue Nachricht verfassen')] = 'success';
  $texts['matches'][t('Send message')] = 'success';
  $texts['matches'][t('Beitreten')] = 'success';

#  // This would also match the string above, however the class returned would
#  // also be the one above; "matches" takes precedence over "contains".
#  $texts['contains'][t('Unique')] = 'bullhorn';

#  // Remove matching for strings that contain "filter":
#  unset($texts['contains'][t('Filter')]);

#  // Change the icon that matches "Upload" (originally "upload"):
#  $texts['contains'][t('Upload')] = 'ok';
}

// for main menu
function bootstrap_adiuto_cdn_preprocess_block(&$vars) {
  #dsm($vars);
  #dsm($vars['block']);
   /* Set shortcut variables */
  $block_id = $vars['block']->module . '_' . $vars['block']->delta;
  /* Add classes based on the block delta */
  switch ($block_id) {
    case 'system_main-menu':
      $vars['classes_array'][] = 'visible-xs-block';
      break;
  }
}


function bootstrap_adiuto_cdn_preprocess_html(&$variables) {
  if (bootstrap_adiuto_cdn_initiative()) {
    $variables['body_attributes_array']['class'][] = 'context-initiative';
  }
  #dsm($variables);
}

function bootstrap_adiuto_cdn_initiative() {
  $patterns = 'initiative/*' . PHP_EOL .
              'node/*/tasks' . PHP_EOL .
              'node/*/members'
  ;
  $path = current_path();
  $path_alias = drupal_lookup_path('alias', $path);
  if(drupal_match_path($path, $patterns) || drupal_match_path($path_alias, $patterns)) {
    return TRUE;
  }
}

/**
 * Overrides bootstrap_menu_local_tasks().
 */
function bootstrap_adiuto_cdn_menu_local_tasks(&$variables) {
  $output = '';
  #dsm($variables);
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    // this is the custom part to change the html see bootstrap_adiuto_cdn_preprocess_page
    if (bootstrap_adiuto_cdn_initiative()) { 
      $variables['primary']['#prefix'] .= '<div class="navbar-default"><ul class="tabs--primary nav nav-pills">';
      $variables['primary']['#suffix'] = '</ul></div>';
    }
    else {
      $variables['primary']['#prefix'] .= '<ul class="tabs--primary nav nav-tabs">';
      $variables['primary']['#suffix'] = '</ul>';
    }
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs--secondary pagination pagination-sm">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}


// http://codekarate.com/blog/removing-fieldset-drupal-7-date-field
function bootstrap_adiuto_cdn_date_combo($variables) {
  return theme('form_element', $variables);
}

function bootstrap_adiuto_cdn_preprocess_views_view(&$variables) {
  
  // https://developers.facebook.com/tools/debug/og/object/
  // If the view has the name "task", add the OG tags
  if ($variables['name'] == 'task_search' && $variables['display_id'] == 'page_1') {
    // Use views to load a list of tasks by initiaitve.
    // TODO: Should we use views here? Use an instance of the search api view.
    $view = views_get_view('shop');
    $view->set_display('attachment_tasks_for_meta_tags');
    $view->set_arguments(array($variables['view']->args[0]));
    $view->pre_execute();
    $view->execute();    
    $tasks = array();
    
    // As we need a flat list of tasks without any markup, we have to loop through
    // the unrendered array to get them.
    foreach($view->result as $row) {
      $tasks[] = "$row->commerce_product_title [".round($row->field_commerce_stock[0]['raw']['value'])."x]";
    }
    $task_list = implode(', ', $tasks);
    
    $og_description = array(
      '#tag' => 'meta',
      '#attributes' => array(
         'property' => 'og:description',
         #'content' => $variables['view']->result[0]->field_field_description_1[0]['rendered']['#markup'],
         'content'  => $task_list,
      ),
    );
    drupal_add_html_head($og_description, 'og_description');
  }
  if ($variables['name'] == 'task') {
    #dsm($variables['view']);
    // Inside this conditional, we define and add our OG tags
#    $og_title = array(
#      '#tag' => 'meta',
#      '#attributes' => array(
#        'property' => 'og:title',
#        'content' => $variables['view']->build_info['substitutions']['%1']
#                     . ' ['.$variables['view']->result[0]->field_commerce_stock[0]['rendered']['#markup'].'x]' ,
#      ),
#    );
#    drupal_add_html_head($og_title, 'og_title');
    
    // Get the title of the group
    $initiative = isset($variables['view']->result[0]->field_og_group_ref_1[0]['rendered']['#markup']) ? $variables['view']->result[0]->field_og_group_ref_1[0]['rendered']['#markup'] : "Initiative";
    
    $og_description = array(
      '#tag' => 'meta',
      '#attributes' => array(
         'property' => 'og:description',
         'content' => $variables['view']->result[0]->field_field_description_1[0]['rendered']['#markup']
                      .' ['.$variables['view']->result[0]->field_commerce_stock[0]['rendered']['#markup'].'x] '
                      .t("By ›@initiaitve‹, till @deadline.", array("@initiaitve" => $initiative,
                                                                     "@deadline"   => strip_tags($variables['view']->result[0]->field_field_deadline_1[0]['rendered']['#markup']))) . " "                      
                      , 
                      
      ),
    );
    drupal_add_html_head($og_description, 'og_description');
#    $og_url = array(
#      '#tag' => 'meta',
#      '#attributes' => array(
#         'property' => 'og:url',
#         'content' => $GLOBALS['base_url'] . '/task/' . $variables['view']->args[0],
#      ),
#    );
#    drupal_add_html_head($og_url, 'og_url');
#    $og_type = array(
#      '#tag' => 'meta',
#      '#attributes' => array(
#         'property' => 'og:type',
#         'content' => 'article',
#      ),
#    );
#    drupal_add_html_head($og_type, 'og_type');
    $og_expiration = array(
      '#tag' => 'meta',
      '#attributes' => array(
         'property' => 'article:expiration_time',
         'content' => $variables['view']->result[0]->field_field_deadline[0]['raw']['value'],
      ),
    );
    drupal_add_html_head($og_expiration, 'og_expiration');
    global $language;
    // TODO: This is dirty, we should do that in a more general way.
    $iso_lang = array('de' => 'de_DE', 'en' => 'en_US');
    $locale = isset($iso_lang[$language->language]) ? $iso_lang[$language->language] : "en_US";
    $og_locale = array(
      '#tag' => 'meta',
      '#attributes' => array(
         'property' => 'og:locale',
         'content' => $locale,
      ),
    );
    drupal_add_html_head($og_locale, 'og_locale');  
    
    // List all installed languages.
    $languages = language_list();  
    unset($languages[$language->language]);
    
    foreach ($languages as $lang) {
      if (isset($iso_lang[$lang->language])) {
        $og_locale_alt = array(
          '#tag' => 'meta',
          '#attributes' => array(
             'property' => 'og:locale:alternate',
             'content' => $iso_lang[$lang->language],
          ),
        );
        drupal_add_html_head($og_locale_alt, 'og_locale_alt');
      }
    }
    $og_image = array(
      '#tag' => 'meta',
      '#attributes' => array(
         'property' => 'og:image',
         'content' => file_create_url($variables['view']->result[0]->field_field_banner[0]['raw']['uri']),
      ),
    );
    drupal_add_html_head($og_image, 'og_image');
  }
}



function bootstrap_adiuto_cdn_facetapi_link_active($variables) {
 
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];
 
  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'], 
    'active' => TRUE,
  );
 
  $variables['text']  = '<span class="pull-right danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>';
  $variables['text']  = '<span class="text-warning"> ' . $variables['text'] . $link_text . '</span>';
 
  $variables['options']['html'] = TRUE;
 
  //array_push($variables['options']['attributes']['class'], "list-group-item");
  //array_push($variables['options']['attributes']['class'], "row");
 
  return theme_link($variables);
}

function bootstrap_adiuto_cdn_facetapi_link_inactive($variables) {
  // Builds accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'], 
    'active' => FALSE,
  );
  $accessible_markup = theme('facetapi_accessible_markup', $accessible_vars);
 
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $variables['text'] = ($sanitize) ? check_plain($variables['text']) : $variables['text'];
 
  // Adds count to link if one was passed.
  if (isset($variables['count'])) {
    $variables['text'] .= '<div class="badge pull-right">' . $variables['count'] . '</div>';
  }
 
  // Resets link text, sets to options to HTML since we already sanitized the
  // link text and are providing additional markup for accessibility.
  $variables['text'] .= $accessible_markup;
  $variables['options']['html'] = TRUE;
 
  //array_push($variables['options']['attributes']['class'], "list-group-item");
  //array_push($variables['options']['attributes']['class'], "row");

  return theme_link($variables);
}

function bootstrap_adiuto_cdn_facetapi_deactivate_widget($variables) {
  return '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
}

